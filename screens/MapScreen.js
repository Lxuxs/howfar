import { Text, View } from 'react-native';
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";

export function MapScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Header/>
        <Text>Map Screen</Text>
        <Footer/>
      </View>
    );
  }