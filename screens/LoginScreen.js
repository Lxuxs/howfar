import { View } from 'react-native';
import { LoginForm } from "../components/authentification/LoginForm";
import { Header } from "../components/Header";
import { Footer } from '../components/Footer';

export function LoginScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Header/>
        <LoginForm/>
        <Footer/>
      </View>
    );
  }