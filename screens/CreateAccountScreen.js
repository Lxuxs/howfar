import React from 'react';
import { View, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import IconButton from '../components/utils/IconButton';
import ErrorMessage from '../components/ErrorMessage';
import InputField from '../components/utils/InputField';
import { Header } from '../components/Header';
import { Footer } from '../components/Footer';
export { IconButton, ErrorMessage, InputField };

export function CreateAccountScreen({ navigation }) {
  
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Header/>
        <Text style={styles.container}>CreateAccount Screen</Text>
        <TextInput
          style={styles.containerInput}
          underlineColorAndroid="transparent"
          placeholder="Username"
          placeholderTextColor="black"
          autoCapitalize="none"
        />
         <TextInput
          style={styles.containerInput}
          underlineColorAndroid="transparent"
          placeholder="Password"
          placeholderTextColor="black"
          autoCapitalize="none"
          secureTextEntry={true}
        />
        <TouchableOpacity style={styles.submitButton}>
          <Text>Submit</Text>
        </TouchableOpacity>
        <Footer/>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
    backgroundColor: '#95a5a6',
    padding:12,
    width: 120,
    margin: 5,
    borderRadius: 5,
    alignItems: 'center',
  },
  containerInput:{
    marginVertical: 20,
    backgroundColor: '#bdc3c7',
    padding:12,
    width: 120,
    margin: 5,
    borderRadius: 5,
    alignItems: 'center',
  },
  submitButton:{
    marginVertical: 20,
    backgroundColor: '#3498db',
    padding:12,
    width: 120,
    margin: 5,
    borderRadius: 5,
    alignItems: 'center',
  },
});