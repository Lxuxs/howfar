import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Footer } from '../components/Footer';
import { Header } from "../components/Header";

export function ProfilScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Header/>
        <TouchableOpacity style={styles.container} onPress={() => navigation.navigate('Login')} > 
          <Text style={styles.text}>Go to Login</Text>   
        </TouchableOpacity>

        <TouchableOpacity style={styles.container} onPress={() => navigation.navigate('CreateAccount')} >
          <Text style={styles.text}>Go to CreateAccount</Text>
        </TouchableOpacity>
        <Footer/>
      </View>
    );
  }
const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
    backgroundColor: '#95a5a6',
    padding:12,
    width: 120,
    margin: 5,
    borderRadius: 5,
  }
})