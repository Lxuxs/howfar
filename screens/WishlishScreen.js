import { Text, View } from 'react-native';
import { Footer } from '../components/Footer';
import { Header } from "../components/Header";

export function WishlistScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Header/>
        <Text>Wishlist</Text>
        <Footer/>
      </View>
    );
  }